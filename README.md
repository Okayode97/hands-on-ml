# Python Implementation of Hands on Machine Learnng with Scikit-Learn & TensorFlow
Covers Part 1 of the Book
- Chapter 1 The Machine Learning Landscape
- Chapter 2 End-to-End Machine Learning Project
- Chapter 3 Classification
- Chapter 4 Training Models
- Chapter 5 Support Vector Machines
- Chapter 6 Decision Trees
- Chapter 7 Ensemble Learning and Random Forests
- Chapter 8 Dimensionality Reduction